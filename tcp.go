package basil

import (
	"log"
	"net"
	"time"
)

// TODO implement configurable servers as alternatives

func ListenAndServeTCP(addr string, handler Handler) error {
	address, err := net.ResolveTCPAddr("tcp4", addr)
	if err != nil {
		return err
	}

	listener, err := net.ListenTCP("tcp4", address)
	if err != nil {
		return err
	}

	for {
		conn, err := listener.AcceptTCP()
		if err != nil {
			log.Println(err)
		} else {
			go func() {
				defer log.Println("dropping connection")
				defer conn.Close()
				conn.SetKeepAlive(true)
				conn.SetKeepAlivePeriod(time.Second * 10)

				handler.ServeMQTT(conn)
			}()
		}
	}
}

// func ListenAndServeTLS(addr string, handler Handler, config tls.Config) error {
// 	tlsListener, err := tls.Listen("tcp4", addr, &config)
// 	if err != nil {
// 		return err
// 	}
//
// 	listener, ok := tlsListener.(*net.TCPListener)
// 	if !ok {
// 		return errors.New("failed to create tls listener")
// 	}
//
// 	for {
// 		conn, err := listener.AcceptTCP()
// 		if err != nil {
// 			log.Println(err)
// 		} else {
// 			go func() {
// 				defer conn.Close()
// 				conn.SetKeepAlive(true)
// 				conn.SetKeepAlivePeriod(time.Second * 10)
//
// 				handler.ServeMQTT(conn)
// 			}()
// 		}
// 	}
// }
