package basil

import "io"

// Handler is a simple interface for passing network connections off
// to a MQTT router and connection handler. This function should block
// in some way as the network connection will be closed when this function
// returns. This makes it very easy to handle the connection state since
// disconnecting is as simple as sending a disconnect message and then
// returning, therein dropping the connection.
type Handler interface {
	ServeMQTT(io.ReadWriter)
}
