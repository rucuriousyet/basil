// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// plugins are managed at either a node level or cluster level
// depending on if the node is part of a cluster or not and has
// leader-control

// pluginCmd represents the plugin command
var pluginCmd = &cobra.Command{
	Use:   "plugin",
	Short: "Commands for managing plugins",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("plugin called")
	},
}

func init() {
	pluginCmd.AddCommand(&cobra.Command{
		Use: "ls",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("ls called")
		},
	})

	pluginCmd.AddCommand(&cobra.Command{
		Use: "ps",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("ps called")
		},
	})

	pluginCmd.AddCommand(&cobra.Command{
		Use: "disable",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("disable called")
		},
	})

	pluginCmd.AddCommand(&cobra.Command{
		Use: "enable",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("enable called")
		},
	})

	pluginCmd.AddCommand(&cobra.Command{
		Use: "add",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("add called")
		},
	})

	pluginCmd.AddCommand(&cobra.Command{
		Use: "rm",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("rm called")
		},
	})

	rootCmd.AddCommand(pluginCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// pluginCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// pluginCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
