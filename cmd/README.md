# basil MQTT broker

> This is the canonical implementation of the basil MQTT library.
> Use this instead of the library if you only require common MQTT functionality
> or prefer to run a broker standalone.

### TODO
+ Gopher-Lua Plugins for Auth
+ Config using YAML, JSON or HCL
+ Prometheus Metrics
+ Logging
