package basil

import (
	"fmt"
	"io"

	"gitlab.com/rucuriousyet/basil/pkt"
)

type PubMSG struct{}

type Session struct{}

type Topic struct{}

type Mux struct {
	// msg queue
	msgs   []PubMSG
	topics map[string]*Topic

	sessions map[string]Session
}

type MuxOpts struct{}

func New(opts *MuxOpts) (*Mux, error) {
	return nil, nil
}

func decodeLength(bytes []byte) (int, int) {
	a := bytes[0] & 127
	left := 3
	var b, c, d byte

	if int(bytes[0]&128) != 0 {
		b = bytes[1] & 127
		left--
		if int(bytes[1]&128) != 0 {
			c = bytes[2] & 127
			left--
			if int(bytes[2]&128) != 0 {
				left--
				d = bytes[3] & 127
			}
		}
	}

	return int(a) + (int(b) * 128) + int((uint32(c)*16384)+(uint32(d)*2097152)), left
}

func (m *Mux) ServeMQTT(conn io.ReadWriter) {
	for {
		fixedHeader := make([]byte, 1)
		n, err := io.ReadFull(conn, fixedHeader)
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		if n != len(fixedHeader) {
			fmt.Println("packet too short")
			return
		}

		packetType := pkt.PacketType(fixedHeader[0] >> 4)
		switch packetType {
		case pkt.Connect:
			if !hasSession {

			}
			break

		case pkt.Disconnect:
			if hasSession {

			}

			break

		default:
			fmt.Println("unrecognized packet type")
			return
		}
	}

	// fixedHeader := make([]byte, 5)
	// io.ReadFull(conn, fixedHeader)
	// packetType := pkt.PacketType(fixedHeader[0] >> 4)
	//
	// packetIsDuplicate := (fixedHeader[0]>>3)&0x01 > 0
	// packetQOS := (fixedHeader[0] >> 1) & 0x03
	// packetShouldRetain := fixedHeader[0]&0x01 > 0
	// packetRemainingLength, bytesLeftover := decodeLength(fixedHeader[1:])
	//
	// packetBody := make([]byte, packetRemainingLength-bytesLeftover)
	// io.ReadFull(conn, packetBody)
	//
	// if bytesLeftover > 0 {
	// 	packetBody = append(fixedHeader[2+(3-bytesLeftover):], packetBody...)
	// }
	//
	// fmt.Println(
	// 	packetType,
	// 	packetIsDuplicate,
	// 	packetQOS,
	// 	packetShouldRetain,
	// 	packetRemainingLength,
	// 	fixedHeader,
	// 	packetBody,
	// )
}
