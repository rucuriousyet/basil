package basil

import (
	"bytes"
	"io"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

type WSReadWriter struct {
	Conn *websocket.Conn
}

func (w *WSReadWriter) Read(p []byte) (int, error) {
	_, data, err := w.Conn.ReadMessage()
	readIndex := int64(0)

	if readIndex >= int64(len(data)) {
		return 0, io.EOF
	}

	n := copy(p, data[readIndex:])
	readIndex += int64(n)

	return n, err
}

func (w *WSReadWriter) Write(p []byte) (int, error) {
	return len(p), w.Conn.WriteMessage(websocket.BinaryMessage, p)
}

func WebsocketHandler(handler Handler) http.HandlerFunc {
	upgrader := websocket.Upgrader{
		CheckOrigin: func(req *http.Request) bool { return true },
	} // use default options

	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		conn, err := upgrader.Upgrade(res, req, nil)
		if err != nil {
			log.Println("failed to upgrade connection", err.Error())
			return
		}

		go func() {
			defer log.Println("dropping connection")
			defer conn.Close()

			// read in all data to prevent fragmented
			// frames
			var buf bytes.Buffer
			io.Copy(&buf, &WSReadWriter{conn})

			handler.ServeMQTT(&buf)
		}()
	})
}
