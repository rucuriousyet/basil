package main

import (
	"net/http"

	"gitlab.com/rucuriousyet/basil"
	goji "goji.io"
	"goji.io/pat"
)

func main() {
	httpMux := goji.NewMux()

	httpMux.HandleFunc(pat.Get("/mqtt"), basil.WebsocketHandler(&basil.Mux{}))

	go basil.ListenAndServeTCP("localhost:5000", &basil.Mux{})
	go basil.ListenAndServeTCP("localhost:4000", &basil.Mux{})
	http.ListenAndServe("localhost:8080", httpMux)
}
