const WebSocket = require('websocket').w3cwebsocket;
// for browser native compat ^^^

const sock = new WebSocket('ws://localhost:8080/echo');

const ab2str = (buf) => {
	  return String.fromCharCode.apply(null, new Uint8Array(buf));
};

sock.onmessage = (e) => {
  console.log(e.data, ab2str(e.data));
};

sock.onopen = () => {
  console.log('connection created');
  sock.send(Uint8Array.from(Buffer.from('hello from node')).buffer);
};

sock.onclose = () => {
  console.log('connection closed');
};
