test:
	go test -v .

install_caddy_linux:
	curl https://getcaddy.com | bash -s personal

publish_test:
	MQTT_HOST="localhost" MQTT_PORT="4000" MQTT_USERNAME="user" MQTT_PASSWORD="password" mqttcli sub -t "hellotopic" -m "bananas"
