package pkt

type PacketType byte
type QOS byte

const (
	AtMostOnce  QOS = 0
	AtLeastOnce QOS = 1
	ExactlyOnce QOS = 2

	// Connect is a client request to connect
	Connect PacketType = 1

	// Connack is a connect acknowledgment
	Connack PacketType = 2

	// Publish is a publish message
	Publish PacketType = 3

	// Puback is a publish acknowledgment
	Puback PacketType = 4

	// Pubrec is a publish received (assured delivery part 1)
	Pubrec PacketType = 5

	// release
	Pubrel PacketType = 6

	// complete
	Pubcomp PacketType = 7

	Subscribe PacketType = 8
	Suback    PacketType = 9

	Unsubscribe PacketType = 10
	Unsuback    PacketType = 11

	// Pingreq is a PING request
	Pingreq PacketType = 12

	// Pingresp is a PING response
	Pingresp PacketType = 13

	// Disconnect is a client disconnecting
	Disconnect PacketType = 14
)

func (p PacketType) String() string {
	switch p {
	case Connect:
		return "connect"
	case Connack:
		return "connack"
	case Publish:
		return "publish"
	case Puback:
		return "puback"
	case Pubrec:
		return "pubrec"
	case Pubrel:
		return "pubrel"
	case Pubcomp:
		return "pubcomp"
	case Subscribe:
		return "subscribe"
	case Suback:
		return "suback"
	case Unsubscribe:
		return "unsubscribe"
	case Unsuback:
		return "unsuback"
	case Pingreq:
		return "pingreq"
	case Pingresp:
		return "pingresp"
	case Disconnect:
		return "disconnect"
	}

	return ""
}

type Header struct {
	Type            PacketType
	Dup             bool
	QOS             QOS
	Retain          bool
	RemainingLength int
}

func HeaderFromByte(data byte) (*Header, error) {
	return &Header{
		// Type:   data >> 4,
		// Dup:    (data>>3)&0x01 > 0,
		// QOS:    (data >> 1) & 0x03,
		// Retain: data&0x01 > 0,
	}, nil
}
